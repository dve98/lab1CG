###LAB 1 Computacion grafica
import math



class Vector:
    n=0
    valores=[]
  
    def __init__(self,n,valores):
        self.valores=valores
        self.n=n
    def obtenerVector(self):
        return self.valores

    def obtenerN(self):
        return self.n

    def multiplicacionEscalar(self,escalar):
        for x in range(0,self.n):
            self.valores[x]=escalar * self.valores[x]

    def divisionEscalar(self,escalar):
        for x in range(0,self.n):
            self.valores[x]= self.valores[x]/escalar 

    def sumarVector(self,otroVector):

        try:
            for x in range(0,self.n):
                self.valores[x]= otroVector.obtenerVector()[x] + self.valores[x]
        except:
            print("No se puede sumar el vector ingresado")


    def restarVector(self,otroVector):
        if self.n!=otroVector.obtenerN():
            raise Exception("Los vectores tienen N diferente")
        try:
            for x in range(0,self.n):
                self.valores[x]= self.valores[x]-otroVector.obtenerVector()[x] 
        except:
            print("No se puede restar el vector ingresado")
    
    def crossProduct(self, otroVector):
        resultado=[]
        resultado.append(self.valores[1] * otroVector.obtenerVector()[2] - self.valores[2] * otroVector.obtenerVector()[1])
        resultado.append(self.valores[2] * otroVector.obtenerVector()[0] - self.valores[0] * otroVector.obtenerVector()[2])
        resultado.append(self.valores[0] * otroVector.obtenerVector()[1] - self.valores[1] * otroVector.obtenerVector()[0])
        return resultado

    def productoPunto(self, otroVector):
 
        producto = 0
 
        for i in range(0, self.n):
            producto = producto + self.valores[i] * otroVector.obtenerVector()[i]

        return producto

    def norma(self):
        suma=0
        for x in self.valores:
            suma=suma+x**2
        return suma**(1/2)

    def calcularAngulo(self,otroVector):
        productoPunto=self.productoPunto(otroVector)
        normaSelf=self.norma()
        normaOtroVector=otroVector.norma()
        resultado=productoPunto/(normaOtroVector*normaSelf)

        return math.degrees(math.acos(resultado))

    def normalizar(self):
        normaSelf=self.norma()
        
        self.divisionEscalar(normaSelf)
      

#####Proyectiol#########
##pos es un objeto de la clase vector, el angulo esta en grados.
def lanzamiento(vin,dt,pos,angulo):
    math.radians(angulo)
    gravedad=9.8
    angulo=math.radians(angulo)
    posxinicial=pos.obtenerVector()[0]
    posyinicial=pos.obtenerVector()[1]
    viy=vin*math.sin(angulo)
    vix=vin*math.cos(angulo)
    tmax=(viy+((viy**2) +2*gravedad*posyinicial)**(1/2))/gravedad
    tiempo=dt
    posx= vix*tiempo + posxinicial
    posy=viy*tiempo - (gravedad*(tiempo**2))/2 + posyinicial
    while(posy>0.001):
        
        posx= round(vix*tiempo +posxinicial,2)
        posy=round(viy*tiempo - (gravedad*(tiempo**2))/2 +posyinicial,2)
        print('Tiempo: '+str(round(tiempo,2))+' segundos '+' Posicion: x: '+str(posx)+'  y: '+str(round(posy,2)))
        tiempo=tiempo+dt
        if(tiempo>tmax):tiempo=tmax
    return tiempo


posicion=Vector(3,[0,500,0])
vo=300
dt=1
angulo=0

lanzamiento(vo,dt,posicion,angulo)